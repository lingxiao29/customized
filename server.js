const express = require("express");
const app = express();
const port = 3000;
// const port = 3030;
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const fs = require("fs");

// To create a virtual path prefix (where the path does not actually exist
// in the file system) for files that are served by the express.static
// function, specify a mount path for the static directory:
app.use("/js", express.static(__dirname + "/www"));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/app.html");
  // res.sendFile(__dirname + '/index.html');
});

io.on("connection", (socket) => {
  // console.log("a user connected");
  socket.on("ratingInfo", (userData) => {
    // console.log(userData);
    let rateData = userData + "\n";
    // fs.appendFile("log.txt", rateData, function (err) {
    fs.writeFile("log.txt", rateData, { flag: "a" }, function (err) {
      if (err) {
        // append failed
      } else {
        // done
      }
    });
  });
});

server.listen(port, () => {
  console.log(`Server listening at ${port}`);
});
