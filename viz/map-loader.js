"use strict";

const parse = require("parse-grid-bench");
const ndarray = require("ndarray");
const toBoxes = require("bitmap-to-boxes");
const nets = require("nets");
const createRenderer = require("./render");
const files = require("./meta.json");

module.exports = createMapLoader;

function grabFile(url, cb) {
  var burl = "https://mikolalysenko.github.io/sturtevant-grid-benchmark/" + url.slice(1);
  nets({ url: burl, encoding: "utf-8" }, function (err, resp, body) {
    cb(err, body);
  });
}

function createMapLoader() {
  let codeIdx;
  let mapIdx = 0;
  let fileNames = shuffle(Object.keys(files));
  let scaleList = {
    arena: 14,
    arena2: 3,
    AR0702SR: 8,
    AR0013SR: 5,
    AR0510SR: 6,
    ht_0_hightown_a2: 2,
    AR0701SR: 1,
    hillsofglory: 2,
    scorchedbasin: 1,
  };

  let currAlgo;
  let algo = ["A", "B", "C"];

  let nextMapFlag = false;
  let rateCounter = 0;

  const canvas = document.createElement("canvas");
  canvas.width = 256;
  canvas.height = 256;

  const renderer = createRenderer([32, 32], canvas);
  renderer.scenario = [];

  renderer.query = 0;

  renderer.pl = false;
  renderer.sim = false;
  renderer.pen = false;

  let boxes = [];

  const headerDiv = document.createElement("div");
  headerDiv.innerHTML = "<h3>- Alternative Paths Ratings for User-selected Queries -</h3>";
  headerDiv.style.position = "absolute";
  headerDiv.style["text-align"] = "center";
  headerDiv.style.left = "0";
  headerDiv.style.right = "0";
  headerDiv.style.top = "0";
  document.body.appendChild(headerDiv);

  const mapDiv = document.createElement("p");
  mapDiv.style.position = "absolute";
  mapDiv.style.left = "5%";
  mapDiv.style.top = "35px";
  mapDiv.style.width = "90%";
  mapDiv.style.height = "30px";

  let mDiv = document.createElement("div");
  mDiv.style.display = "inline-block";
  mDiv.style.width = "350px";
  mDiv.innerHTML = "Map:  " + fileNames[mapIdx] + " | " + (fileNames.length - mapIdx) + " maps left";
  mapDiv.appendChild(mDiv);

  scaleMap(mapIdx);

  // zoom in and out
  let zoom = document.createElement("div");
  zoom.style.display = "inline";
  zoom.style.marginLeft = "10px";
  zoom.innerHTML = "Map Scale:";
  mapDiv.appendChild(zoom);

  var zmSelect = document.createElement("select");
  let zoomList = [1, 2, 3, 5, 6, 8, 14];
  for (let i of zoomList) {
    zmSelect.options.add(new Option("x " + i, i));
  }

  zmSelect.value = scaleList[fileNames[mapIdx]] | 0;
  zmSelect.style.display = "inline";
  zmSelect.style.margin = "5px";
  zmSelect.addEventListener("change", function () {
    let ms = zmSelect.value | 0;
    renderer.canvas.width = renderer.shape[0] * ms;
    renderer.canvas.height = renderer.shape[1] * ms;
  });
  mapDiv.appendChild(zmSelect);
  // zoom in and out

  let mpbtn = document.createElement("button");
  mpbtn.innerHTML = "Next Map";

  mpbtn.style.width = "98px"; // Single button
  mpbtn.style.marginLeft = "290px";
  mpbtn.addEventListener("click", function () {
    if (nextMapFlag) {
      mapIdx++;
      canvas.style.pointerEvents = "auto";
      if (mapIdx === fileNames.length) {
        // mapIdx = 0;
        mpbtn.disabled = true;
        sbtn.disabled = true;
        adl.disabled = true;
        bdl.disabled = true;
        cdl.disabled = true;
        arb.disabled = brb.disabled = crb.disabled = true;
        zmSelect.disabled = true;
        commbx.value = "";
        commbx.disabled = true;
        mDiv.innerHTML = "Map";
        renderer.tipMessage("Thank you very much for completing the survey. You can close this window now.");
      } else {
        mapChange(mapIdx);
      }
    } else if (renderer.query === 0) {
      renderer.tipMessage("INSTRUCTION: Please rate at least one query on this map before moving to the next map.");
    }
  });
  mapDiv.appendChild(mpbtn);

  document.body.appendChild(mapDiv);

  // Second row
  var rateDiv = document.createElement("p");
  rateDiv.style.position = "absolute";
  rateDiv.style.left = "5%";
  rateDiv.style.top = "70px";
  rateDiv.style.width = "90%";
  rateDiv.style.height = "30px";

  var title = document.createElement("div");
  title.style.display = "inline-block";
  title.style.width = "110px";
  title.innerHTML = "Approaches |";
  rateDiv.appendChild(title);

  // Approach A
  let arb = document.createElement("input");
  let brb = document.createElement("input");
  let crb = document.createElement("input");
  let radioList = [arb, brb, crb];
  algo.forEach((algoName, j) => {
    // console.log("selected algorithm: ", algoName);
    let labelValue = document.createElement("label");
    labelValue.innerHTML = algoName + ":";
    let codeSelect = radioList[j];
    codeSelect.type = "radio";
    codeSelect.style.marginRight = "20px";
    codeSelect.name = "algorithms";
    codeSelect.value = algoName;
    rateDiv.appendChild(labelValue);
    rateDiv.appendChild(codeSelect);

    codeSelect.addEventListener("click", function () {
      // console.log("selected algorithm: ", algoName);
      dropList[j].value = "";
      renderer.search = algoName;
      renderer.events.emit("planner-change");
    });
  });

  getRandomAlgo();

  // console.log("current algo :", currAlgo);

  // Rating section
  let rtDiv = document.createElement("div");
  rtDiv.style.display = "inline";
  rtDiv.style.marginLeft = "88px"; // Chrome
  rtDiv.style.marginRight = "6px";
  rtDiv.innerHTML = "* Ratings |";
  rateDiv.appendChild(rtDiv);

  let adl = document.createElement("select");
  let bdl = document.createElement("select");
  let cdl = document.createElement("select");
  let dropList = [adl, bdl, cdl];
  algo.forEach((approName, i) => {
    let labelValue = document.createElement("label");
    labelValue.innerHTML = approName + ":";
    rateDiv.appendChild(labelValue);
    let rtSelect = dropList[i];
    for (let i = 1; i <= 5; i++) {
      rtSelect.options.add(new Option(i, i));
    }
    rtSelect.value = "";
    rtSelect.style.display = "inline-block";
    rtSelect.style.width = "50px"; // chrome
    rtSelect.style.marginLeft = "5px";
    rtSelect.style.marginRight = "20px";
    rateDiv.appendChild(rtSelect);
  });

  document.body.appendChild(rateDiv);

  // Comments row
  var commDiv = document.createElement("p");
  commDiv.style.position = "absolute";
  commDiv.style.left = "5%";
  commDiv.style.top = "105px";
  commDiv.style.width = "90%";
  commDiv.style.height = "30px";

  let commlb = document.createElement("label");
  commlb.innerHTML = "Comments (optional): ";
  commDiv.appendChild(commlb);

  var commbx = document.createElement("input");
  // commbx.setAttribute("type", "text");
  commbx.type = "text";
  commbx.style.width = "530px";
  commDiv.appendChild(commbx);

  // Submit button
  let sbtn = document.createElement("button");
  sbtn.innerHTML = "Submit";
  sbtn.style.marginLeft = "98px";
  sbtn.style.width = "98px"; // Single button
  commDiv.appendChild(sbtn);
  sbtn.addEventListener("click", function () {
    if (renderer.query === 0) {
      renderer.tipMessage("INSTRUCTION: Please click on the map and choose a start and a target.");
    } else if (adl.value == "" || bdl.value == "" || cdl.value == "") {
      renderer.tipMessage("INSTRUCTION: Please rate all three approaches and then save the ratings.");
    } else if (!(renderer.pl && renderer.sim && renderer.pen)) {
      printIntro();
    } else {
      renderer.mapNum = mapIdx;
      renderer.search = currAlgo;
      renderer.rating = adl.value + "_" + bdl.value + "_" + cdl.value;
      renderer.mapName = fileNames[mapIdx];
      renderer.comment = commbx.value;
      renderer.events.emit("rate-send");

      adl.value = bdl.value = cdl.value = "";
      renderer.tipMessage(
        "INSTRUCTION: Thank you for your ratings. We have recevied them. " +
          " You can try out more queries on this map or move to the next map."
      );

      getRandomAlgo();

      renderer.pl = false;
      renderer.sim = false;
      renderer.pen = false;

      adl.value = bdl.value = cdl.value = commbx.value = "";

      renderer.rating = undefined;
      renderer.events.emit("query-change");

      nextMapFlag = true;

      renderer.query = 0;
    }
  });

  document.body.appendChild(commDiv);

  // Result Div, third row
  var resultDiv = document.createElement("p");
  resultDiv.style.position = "absolute";
  resultDiv.style.left = "5%";
  resultDiv.style.top = "140px";
  resultDiv.style.width = "90%";
  resultDiv.style.height = "30px";

  var timeDiv = document.createElement("div");
  timeDiv.style.display = "inline";
  resultDiv.appendChild(timeDiv);
  renderer.logMessage = function (str) {
    timeDiv.innerHTML = str;
  };
  document.body.appendChild(resultDiv);

  // Instruction
  var tipsDiv = document.createElement("p");
  tipsDiv.style.position = "absolute";
  tipsDiv.style.left = "5%";
  tipsDiv.style.top = "175px";
  tipsDiv.style.width = "90%";
  tipsDiv.style.height = "30px";

  var insDiv = document.createElement("div");
  insDiv.style.display = "inline";
  insDiv.style.color = "red";
  tipsDiv.appendChild(insDiv);
  renderer.tipMessage = function (str) {
    insDiv.innerHTML = str;
  };
  document.body.appendChild(tipsDiv);

  // Canvas Div
  var canvasDiv = document.createElement("div");
  canvasDiv.style.position = "absolute";
  canvasDiv.style.left = "5%";
  canvasDiv.style.bottom = "25px";
  canvasDiv.style.top = "240px";
  canvasDiv.style.width = "90%";
  canvasDiv.style.overflow = "scroll";
  canvasDiv.appendChild(canvas);
  document.body.appendChild(canvasDiv);

  var data = ndarray(new Uint8Array(32 * 32), [32, 32]);

  function getRandomAlgo() {
    // codeIdx = getRandomInt(3);
    codeIdx = 0;
    currAlgo = algo[codeIdx];
    renderer.search = currAlgo;
    radioList[codeIdx].checked = true;
  }

  function printIntro() {
    if (!renderer.pl) {
      renderer.tipMessage(
        "INSTRUCTION: You have not viewed the paths generated by approach A " +
          "but have entered a rating for it. Please select its radio button " +
          "to view its generated paths before entering the rating."
      );
    } else if (!renderer.sim) {
      renderer.tipMessage(
        "INSTRUCTION: You have not viewed the paths generated by approach B " +
          "but have entered a rating for it. Please select its radio button " +
          "to view its generated paths before entering the rating."
      );
    } else if (!renderer.pen) {
      renderer.tipMessage(
        "INSTRUCTION: You have not viewed the paths generated by approach C " +
          "but have entered a rating for it. Please select its radio button " +
          "to view its generated paths before entering the rating."
      );
    }
  }

  function getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }

  function scaleMap(num) {
    // let scale = scaleList[num] | 0;
    let scale = scaleList[fileNames[num]] | 0;
    renderer.canvas.width = renderer.shape[0] * scale;
    renderer.canvas.height = renderer.shape[1] * scale;
    if (num >= 1) zmSelect.value = scale;
  }

  function mapChange(mid) {
    adl.value = bdl.value = cdl.value = commbx.value = "";
    renderer.query = 0;
    nextMapFlag = false;

    var file = files[fileNames[mid]];
    renderer.tipMessage(" Loading Map -" + fileNames[mid] + ".....");
    function handleError(err) {
      renderer.tipMessage("Error loading map data");
    }
    grabFile(file.map, function (err, mapData) {
      if (err) {
        handleError(err);
        return;
      }

      var map = parse.map(mapData);

      if (!map) {
        handleError(err);
        return;
      }

      boxes = toBoxes(map.transpose(1, 0), true);
      renderer.grid = map;
      renderer.shape = map.shape.slice();

      scaleMap(mid);
      renderer.scenario = [];

      renderer.pl = false;
      renderer.sim = false;
      renderer.pen = false;

      if (fileNames.length - mid >= 2)
        mDiv.innerHTML = "Map:  " + fileNames[mid] + " | " + (fileNames.length - mid) + " maps left";
      else mDiv.innerHTML = "Map:  " + fileNames[mid] + " | " + (fileNames.length - mid) + " map left";

      renderer.events.emit("data-change");
    });
  }

  function shuffle(array) {
    let currentIndex = array.length,
      randomIndex;

    // While there remain elements to shuffle...
    while (currentIndex != 0) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
    }

    return array;
  }

  renderer.grid = data;

  function render() {
    var ctx = renderer.context;
    ctx.fillStyle = "#fff";
    ctx.fillRect(0, 0, renderer.canvas.width, renderer.canvas.height);

    ctx.fillStyle = "#8f92a2";
    var r = renderer.tileDim();
    for (var i = 0; i < boxes.length; ++i) {
      var b = boxes[i];
      ctx.fillRect(r * b[0][0], r * b[0][1], r * (b[1][0] - b[0][0]), r * (b[1][1] - b[0][1]));
    }
    renderer.events.emit("render");
    requestAnimationFrame(render);
  }

  mapChange(mapIdx);

  requestAnimationFrame(render);

  return renderer;
}
