"use strict";

const now = require("right-now");
const createEditor = require("./map-loader");
const createPlanner = require("../lib/planner");

var io = require("socket.io-client");
// var socket = io.connect("http://localhost:3000", { reconnect: true });
var socket = io.connect("http://118.138.244.173:3000/", { reconnect: true });

const editor = createEditor();
let planner;

let src = [-10, -10];
let dst = [-10, -10];
let path = [];

let userData = "";
let start = [-10, -10];
let goal = [-10, -10];

const M = 3,
  N = 4;
let costInfo = Array.from(Array(M), () => new Array(N));
let nodeInfo = Array.from(Array(M), () => new Array(N));
let overlapList = {};

function sendRating() {
  if (editor.rating !== undefined) {
    userData = new Date().toISOString() + "," + editor.mapNum + ",";
    userData += editor.mapName + "," + start.join("_") + "," + goal.join("_") + ",";
    userData += editor.rating + ",";
    userData += costInfo[0].join("_") + "," + costInfo[1].join("_") + "," + costInfo[2].join("_") + ",";
    userData += nodeInfo[0].join("_") + "," + nodeInfo[1].join("_") + "," + nodeInfo[2].join("_") + ",";
    userData += overlapList[0].join("_") + "," + overlapList[1].join("_") + "," + overlapList[2].join("_") + "~";
    userData += editor.comment;

    socket.on("connect", function (socket) {
      // console.log("Connected!");
    });
    socket.emit("ratingInfo", userData);
  } else {
    // msg
  }
}

function calcPath(v) {
  editor.logMessage("Processing....");
  path = [];
  path.length = 0;

  if (src[0] < 0 || dst[0] < 0) {
    editor.logMessage("...");
    return;
  } else {
    editor.tipMessage(
      "INSTRUCTION: Please either rate all three approaches and then save " +
        "the ratings or choose a new start and target."
    );
    start[0] = src[0];
    start[1] = src[1];
    goal[0] = dst[0];
    goal[1] = dst[1];
  }

  // console.log("Benchmark algo : ", editor.search);

  let cIdx = 0;
  let pathLength = [];
  const pathColor = ["Green", "Blue", "Purple", "Red", "black"];
  if (editor.search === "A") {
    cIdx = 0;
    editor.pl = true;
    let paRes = planner.pl(src[0], src[1], dst[0], dst[1], path, N, 1.5);
    pathLength = paRes[0];
    // console.log("A bench: ", paRes[1]);
    overlapList[0] = paRes[1];
  } else if (editor.search === "B") {
    // let ol = editor.threshold / 10;
    // console.log("Threshold bench: ", ol);
    cIdx = 1;
    editor.sim = true;
    let simRes = planner.sim(src[0], src[1], dst[0], dst[1], path, 0.4, N, 1.5);
    pathLength = simRes[0];
    // console.log("B bench: ", simRes[1]);
    overlapList[1] = simRes[1];
  } else if (editor.search === "C") {
    cIdx = 2;
    editor.pen = true;
    let penRes = planner.pen(src[0], src[1], dst[0], dst[1], path, 1.4, N, 1.5);
    pathLength = penRes[0];
    // console.log("C bench: ", penRes[1]);
    overlapList[2] = penRes[1];
  }

  if (pathLength.length > 0) editor.query = 1;

  // Generate output message
  for (let i = 0; i < N; ++i) {
    costInfo[cIdx][i] = 0;
    nodeInfo[cIdx][i] = 0;
  }
  const pathMap = new Map();
  for (let i = 0; i < pathLength.length; i++) {
    // costInfo[i] = pathLength[i];
    costInfo[cIdx][i] = Math.round(pathLength[i] * 1000 + Number.EPSILON) / 1000;
    nodeInfo[cIdx][i] = path[i].length;
    pathMap.set(i, pathLength[i]);
  }

  var msg = "Lengths of the alternative paths: ";
  if (pathLength.length > 0) {
    let cnt = 0;
    const pathSorted = new Map([...pathMap.entries()].sort((a, b) => a[1] - b[1]));
    for (let [key, value] of pathSorted) {
      msg += "<span style=color:" + pathColor[key] + ">" + pathColor[key] + ": " + Number.parseFloat(value).toFixed(1);
      // msg += "<span style=color:" + pathColor[cnt] + ">" + pathColor[cnt] + ": " + Number.parseFloat(value).toFixed(1);
      cnt += 1;
      if (cnt < pathLength.length) msg += ", ";
    }
  } else {
    msg += "<span style=color:red> Source and Target are not connected!";
  }

  editor.logMessage(msg);
}

function buttonChange(tileX, tileY, buttons) {
  if (buttons) {
    if (src[0] < 0) {
      src[0] = tileX;
      src[1] = tileY;
    } else if (dst[0] < 0) {
      dst[0] = tileX;
      dst[1] = tileY;
    } else {
      src[0] = tileX;
      src[1] = tileY;
      dst[0] = dst[1] = -10;
    }
    calcPath(1);
  }
}

function buildPlanner() {
  src[0] = src[1] = dst[0] = dst[1] = -10;
  editor.logMessage("......");
  path.length = 0;
  planner = createPlanner(editor.grid);
  editor.tipMessage("Instruction: Please click on the map to choose a start and a target.");
  calcPath(0);
}

function refreshPlanner() {
  src[0] = src[1] = dst[0] = dst[1] = -10;
  editor.logMessage("clean query");
  path.length = 0;
  calcPath(1);
}

function drawGeometry() {
  editor.path(path, "#fa0505");
  editor.ends(src[0], src[1], "#0f0");
  editor.ends(dst[0], dst[1], "#f00");
}

buildPlanner();

editor.events.on("render", drawGeometry);
editor.events.on("planner-change", function () {
  calcPath(2);
});
editor.events.on("data-change", buildPlanner);
editor.events.on("query-change", refreshPlanner);
editor.events.on("button-change", buttonChange);
editor.events.on("rate-send", sendRating);
