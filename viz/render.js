"use strict";

var mouseChange = require("mouse-change");
var colormap = require("colormap");
var EventEmitter = require("events").EventEmitter;

module.exports = createRenderer;

function Renderer(context, canvas, shape, events) {
  this.context = context;
  this.canvas = canvas;
  this.shape = shape;
  this.events = events;
}

var proto = Renderer.prototype;

proto.tileDim = function () {
  return Math.round(Math.min(this.canvas.width / this.shape[0], this.canvas.height / this.shape[1])) | 0;
};

proto.tile = function (x, y, color) {
  var tileR = this.tileDim();
  this.context.fillStyle = color || "#fff";
  this.context.fillRect(x * tileR, y * tileR, tileR, tileR);
};

proto.square = function (x, y, color) {
  var tileR = this.tileDim();
  this.context.fillStyle = color || "#fff";
  this.context.fillRect((x + 0.2) * tileR, (y + 0.2) * tileR, tileR * 0.65, tileR * 0.65);
};

proto.circle = function (x, y, color) {
  var tileR = this.tileDim();
  if (tileR <= 2) {
    // this.tile(x, y, color)
    this.tile(x, y, "#6677cc");
    return;
  }
  this.context.fillStyle = color || "#fff";
  this.context.beginPath();
  this.context.arc(x * tileR, y * tileR, 0.25 * tileR, 0, 2.0 * Math.PI);
  this.context.fill();
};

proto.ends = function (x, y, color) {
  let tileR = this.tileDim();
  this.context.fillStyle = color || "#fff";
  this.context.beginPath();
  // this.context.arc(x * tileR, y * tileR, 0.5 * tileR, 0, 2.0 * Math.PI);
  this.context.arc(x * tileR, y * tileR, 5, 0, 2.0 * Math.PI);
  this.context.fill();
};

proto.line = function (ax, ay, bx, by, color) {
  var tileR = this.tileDim();
  this.context.strokeStyle = color || "#fff";
  this.context.beginPath();
  this.context.moveTo(ax * tileR, ay * tileR);
  this.context.lineTo(bx * tileR, by * tileR);
  this.context.lineWidth = 2.5;
  this.context.stroke();
};

// proto.box = function (ax, ay, bx, by, color) {
// this.line(ax - 0.5, ay - 0.5, ax - 0.5, by - 0.5, color);
// this.line(bx - 0.5, ay - 0.5, bx - 0.5, by - 0.5, color);
// this.line(ax - 0.5, ay - 0.5, bx - 0.5, ay - 0.5, color);
// this.line(ax - 0.5, by - 0.5, bx - 0.5, by - 0.5, color);
// };

proto.path = function (route, color) {
  // green, blue, pink, red, black, yellow
  let c = ["#137d2c", "#13367d", "#aa30b3", "#e81026", "#050500", "#fffb05"];
  let pt = [[], [3, 3], [9, 3], [10, 3, 3, 2]];
  for (let k = 0; k < route.length; ++k) {
    let path = route[k];
    this.context.setLineDash(pt[k]);
    for (var i = 0; i + 2 < path.length; i += 2) {
      var sx = path[i];
      var sy = path[i + 1];
      var tx = path[i + 2];
      var ty = path[i + 3];
      this.line(sx, sy, tx, ty, c[k]);
    }
  }
};

function createRenderer(shape, canvas) {
  if (!canvas) {
    canvas = document.createElement("canvas");
    canvas.width = canvas.height = 512;
    document.body.appendChild(canvas);
  }

  var context = canvas.getContext("2d");

  canvas.addEventListener("contextmenu", function (ev) {
    ev.preventDefault();
  });

  var events = new EventEmitter();

  var result = new Renderer(context, canvas, shape, events);

  var lastButton = 0,
    lastTileX = -1,
    lastTileY = -1;
  mouseChange(canvas, function (button, x, y) {
    var tileR = result.tileDim();
    var tileX = Math.floor(x / tileR);
    var tileY = Math.floor(y / tileR);

    if (tileX < 0 || tileY < 0 || tileX >= result.shape[0] || tileY >= result.shape[1]) {
      tileX = tileY = -1;
    }

    if (tileX !== lastTileX || tileY !== lastTileY) {
      events.emit("tile-change", tileX, tileY, button);
    }
    if (button !== lastButton) {
      events.emit("button-change", tileX, tileY, button);
    }

    lastTileX = tileX;
    lastTileY = tileY;
    lastButton = button;
  });

  result.canvas = canvas;

  return result;
}
