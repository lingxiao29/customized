var plateau = "plat";
var dissimilarity = "simi";
var penalty = "pena";

module.exports = {
  Plateau: plateau,
  Dissimilarity: dissimilarity,
  Penalty: penalty,
};
