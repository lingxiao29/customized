"use strict";

function Vertex(x, y) {
  this.x = x;
  this.y = y;
}

let proto = Vertex.prototype;

proto.isEqual = function (u, v) {
  return u.x === v.x && u.y === v.y;
};

function createVertex(x, y) {
  let u = new Vertex(x, y);
  return u;
}

exports.create = createVertex;
