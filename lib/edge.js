"use strict";

// Initializing a class definition
// class Edge {
// constructor(v, w, weight) {
// this.v = v
// this.w = w
// this.weight = weight
// }
// }

// Initializing a constructor function
function Edge(m, n, weight) {
  this.m = m;
  this.n = n;
  this.weight = weight;
}

let proto = Edge.prototype;

/**
 * Return the weight of this edge
 */
proto.getWeight = function () {
  return this.weight;
};

proto.setWeight = function (wt) {
  this.weight = wt;
};

/**
 * Return either endpoint of this edge
 */
proto.either = function () {
  return this.m;
};

/**
 * Return the endpoint of this edge that is different from the given vertex
 */
proto.other = function (u) {
  if (u === this.m) return this.n;
  else if (u === this.n) return this.m;
  else return null;
};

/**
 * Compare two edges by weight
 *
 * @return a negative integer, zero, or positive integer depending on
 *         the weight of this is less than, equal to, or greater than
 *         the argument
 */
proto.compareTo = function (that) {
  if (this.weight < that.weight) return -1;
  else if (this.weight > that.weight) return 1;
  else return 0;
};

function createEdge(m, n, weight) {
  return new Edge(m, n, weight);
}

exports.createEdge = createEdge;
