"use strict";

module.exports = createGeometry;

function Geometry(corners) {
  this.corners = corners;
}

function createGeometry(grid) {
  let corners = [];
  let SE = [1, 1],
    SW = [-1, 1],
    NE = [1, -1],
    NW = [-1, -1];

  let nx = grid.shape[0],
    ny = grid.shape[1];
  for (let y = 1; y < ny - 1; y++) {
    for (let x = 1; x < nx - 1; x++) {
      if (grid.get(x, y) == 0) {
        let a = grid.get(x - 1, y - 1) == 0;
        let b = grid.get(x, y - 1) == 0;
        let c = grid.get(x + 1, y - 1) == 0;
        let d = grid.get(x - 1, y) == 0;
        let e = grid.get(x + 1, y) == 0;
        let f = grid.get(x - 1, y + 1) == 0;
        let g = grid.get(x, y + 1) == 0;
        let h = grid.get(x + 1, y + 1) == 0;

        if (!a && b && d) {
          corners.push([x, y]);
        }
        if (!c && b && e) {
          corners.push([x + 1, y]);
        }
        if (!f && d && g) {
          corners.push([x, y + 1]);
        }
        if (!h && g && e) {
          corners.push([x + 1, y + 1]);
        }

        if (a && !b && !d) {
          corners.push([x, y]);
        }
        if (c && !b && !e) {
          corners.push([x + 1, y]);
        }
        if (f && !d && !g) {
          corners.push([x, y + 1]);
        }
        if (h && !g && !e) {
          corners.push([x + 1, y + 1]);
        }
      }
    }
  }

  return new Geometry(corners);
}
