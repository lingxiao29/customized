"use strict";

var createGeometry = require("./geometry");
var Graph = require("./graphx");
var ndarray = require("ndarray");
var Heap = require("heap");
var ege = require("./edge.js");

module.exports = createPlanner;

function PathPlanner(geometry, graph, grid) {
  this.geometry = geometry;
  this.graph = graph;
  this.grid = grid;
}

var proto = PathPlanner.prototype;

function eucliDist(x0, y0, x1, y1) {
  var dx = Math.abs(x1 - x0);
  var dy = Math.abs(y1 - y0);
  var num = Math.sqrt(dx * dx + dy * dy);
  // return Number(Math.round(num + 'e8') + 'e-8')
  return Number(Math.round(num + "e7") + "e-7");
}

proto.addST = function (sx, sy, tx, ty) {
  let G = this.graph;
  let s_i = -1;
  let t_i = -1;
  let s_f = false;
  let t_f = false;
  for (let i = 0; i < this.geometry.corners.length; ++i) {
    let u = this.geometry.corners[i];
    if (sx === u[0] && sy === u[1]) {
      s_i = i;
      s_f = true;
    }
    if (tx === u[0] && ty === u[1]) {
      t_i = i;
      t_f = true;
    }
  }
  if (!s_f && s_i === -1) {
    var s = G.vertex(sx, sy);
    s_i = G.addEnd(s);
  }
  if (!t_f && t_i === -1) {
    var t = G.vertex(tx, ty);
    t_i = G.addEnd(t);
  }

  if (rayTrace(sx, sy, tx, ty, this.grid)) {
    G.incEdges(s_i, t_i, eucliDist(sx, sy, tx, ty));
  }

  var cl = [];
  for (var k = 0; k < this.geometry.corners.length; ++k) {
    var c = this.geometry.corners[k];
    cl[this.grid.index(c[0], c[1])] = 1;
  }
  for (var i = 0; i < this.geometry.corners.length; ++i) {
    var u = this.geometry.corners[i];
    if (!s_f) {
      if (rayTrace(sx, sy, u[0], u[1], this.grid)) {
        var dist = eucliDist(sx, sy, u[0], u[1]);
        var x0 = u[0],
          y0 = u[1],
          isEdge = true;
        var dx = sx - x0,
          dy = sy - y0;
        var steps = Math.max(Math.abs(dx), Math.abs(dy));
        for (var z = 1; z < steps; z++) {
          if ((z * dx) % steps == 0 && (z * dy) % steps == 0) {
            var x = x0 + (z * dx) / steps,
              y = y0 + (z * dy) / steps;
            if (cl[this.grid.index(x, y)]) isEdge = false;
          }
        }
        if (isEdge) {
          G.incEdges(i, s_i, dist);
        }
      }
    }
    if (!t_f) {
      if (rayTrace(tx, ty, u[0], u[1], this.grid)) {
        var dist = eucliDist(tx, ty, u[0], u[1]);
        var x0 = u[0],
          y0 = u[1],
          isEdge = true;
        var dx = tx - x0,
          dy = ty - y0;
        var steps = Math.max(Math.abs(dx), Math.abs(dy));
        for (var z = 1; z < steps; z++) {
          if ((z * dx) % steps == 0 && (z * dy) % steps == 0) {
            var x = x0 + (z * dx) / steps,
              y = y0 + (z * dy) / steps;
            if (cl[this.grid.index(x, y)]) isEdge = false;
          }
        }
        if (isEdge) {
          G.incEdges(i, t_i, dist);
        }
      }
    }
  }
  return { s: s_i, t: t_i };
};

proto.search = function (sx, sy, tx, ty, out) {
  this.graph.treeClear();
  var pathLenList = [];
  if (this.grid.get(tx, ty) != 0 || this.grid.get(sx, sy) != 0) {
    // return 0;
    return pathLenList;
  } else {
    var G = this.graph;
    G.setSourceAndTarget(sx, sy, tx, ty);
    if (rayTrace(sx, sy, tx, ty, this.grid)) {
      out.push([sx, sy, tx, ty]);
      return eucliDist(sx, sy, tx, ty);
    } else {
      var st = this.addST(sx, sy, tx, ty);
      var p = [];

      var dj = G.findSP(st.s, st.t, p, true);
      out.push(p);
      pathLenList.push(dj);

      G.treeClear();
      // return dj;
      return pathLenList;
    }
  }
};

/**
 * Is it necessary to add s and t for each method?
 */
proto.pl = function (sx, sy, tx, ty, out, tk, ub) {
  // console.log("Plateau is called");
  this.graph.treeClear();
  let res = {};
  res[0] = [];
  res[1] = [];
  if (this.grid.get(tx, ty) != 0 || this.grid.get(sx, sy) != 0) {
    return res;
  } else {
    let G = this.graph;
    G.setSourceAndTarget(sx, sy, tx, ty);
    let st = this.addST(sx, sy, tx, ty);

    G.backTree(st.s, st.t, ub);
    if (typeof G.BT[G.bx[st.s]] === "undefined") return res;

    let dj = G.BT[G.bx[st.s]][1];

    G.forwardTree(st.s, st.t, ub);
    let jj = G.FT[G.fx[st.t]][1];

    let isTa = [];
    let isSo = [];

    let op = [];
    let tuo = [];
    let costs = [];

    for (let i = 0; i < G.verts.length; ++i) isSo[i] = true;

    let nId = st.t;
    let sp = [];
    isSo[G.fx[nId]] = false;
    op.push(nId);
    sp.push(G.verts[nId].x, G.verts[nId].y);
    while (nId != st.s) {
      nId = G.FT[G.fx[nId]][0][1];
      op.push(nId);
      sp.push(G.verts[nId].x, G.verts[nId].y);
      isSo[G.fx[nId]] = false;
    }
    out.push(sp);
    res[0].push(dj);

    tuo.push(op);
    costs.push(dj);

    var cId,
      bpId,
      mId,
      head,
      tail,
      plNodes = 1;
    let heap = new Heap(function (a, b) {
      return b[1] - a[1];
    });
    for (let j = 0; j < G.FT.length; ++j) {
      head = tail = mId = G.FT[j][0][0];
      if (
        typeof G.FT[G.fx[mId]] !== "undefined" &&
        typeof G.BT[G.bx[mId]] !== "undefined" &&
        (G.FT[G.fx[mId]][1] + G.BT[G.bx[mId]][1]) / dj <= ub
      ) {
        if (isSo[G.fx[mId]]) {
          cId = mId;
          var bpId = G.BT[G.bx[mId]][0][1];
          while (isSo[G.fx[bpId]]) {
            if (G.FT[G.fx[bpId]][0][1] === cId) {
              isSo[G.fx[cId]] = false;
              isSo[G.fx[bpId]] = false;
              plNodes++;
              if (plNodes === 2) {
                head = cId;
                tail = bpId;
              } else {
                tail = bpId;
              }
              cId = bpId;
              bpId = G.BT[G.bx[cId]][0][1];
            } else {
              isSo[G.fx[cId]] = false;
              break;
            }
          }
          if (plNodes >= 2) {
            var l = G.FT[G.fx[tail]][1] - G.FT[G.fx[head]][1];
            heap.push([[head, tail], l]);
          } else {
            let m = G.FT[G.fx[cId]][0][1];
            let n = G.BT[G.bx[cId]][0][1];
            if (this.isTaut(G.verts[m].x, G.verts[m].y, G.verts[cId].x, G.verts[cId].y, G.verts[n].x, G.verts[n].y))
              heap.push([[cId, cId], 0]);
          }
        }
      }
      plNodes = 1;
    }

    while (!heap.empty()) {
      if (out.length >= tk) break;
      let h = heap.peek();
      let u = h[0][0];
      let lst = G.route(st.s, u, st.t);
      let arr = G.path(st.s, h[0][0], st.t);
      let pLen = G.FT[G.fx[u]][1] + G.BT[G.bx[u]][1];

      let curol = [];
      for (let i = 0; i < tuo.length; i++) {
        let opv = G.overlapVal(lst, pLen, tuo[i], costs[i]);
        curol.push(opv);
      }

      tuo.push(lst);
      costs.push(pLen);
      res[1].push.apply(res[1], curol);

      res[0].push(pLen);
      out.push(arr);
      heap.pop();
    }

    // console.log("overlap list:", res[1]);

    G.treeClear();
    return res;
  }
};

proto.sim = function (sx, sy, tx, ty, out, val, tk, ub) {
  // console.log("Simi is called");
  this.graph.treeClear();
  let res = {};
  res[0] = [];
  res[1] = [];
  if (this.grid.get(tx, ty) != 0 || this.grid.get(sx, sy) != 0) {
    return res;
  } else {
    let G = this.graph;
    G.setSourceAndTarget(sx, sy, tx, ty);
    let st = this.addST(sx, sy, tx, ty);

    G.backTree(st.s, st.t, ub);
    const dj = G.BT[G.bx[st.s]][1];
    if (typeof G.BT[G.bx[st.s]] === "undefined") return res;

    G.forwardTree(st.s, st.t, ub);
    const jj = G.FT[G.fx[st.t]][1];

    let isTa = [];
    let isSo = [];

    for (var i = 0; i < G.verts.length; ++i) isSo[i] = true;

    let sp = [];
    let tuo = [];
    let costs = [];
    let nId = st.t;
    sp.push(nId);
    isSo[G.fx[nId]] = false;
    while (nId != st.s) {
      nId = G.FT[G.fx[nId]][0][1];
      sp.push(nId);
      isSo[G.fx[nId]] = false;
    }
    tuo.push(sp);
    res[0].push(dj);
    costs.push(dj);

    var cId,
      bpId,
      mId,
      head,
      tail,
      plNodes = 1;
    var heap = new Heap(function (a, b) {
      return a[1] - b[1];
    });
    for (var j = 0; j < G.FT.length; ++j) {
      head = tail = mId = G.FT[j][0][0];
      if (
        typeof G.FT[G.fx[mId]] !== "undefined" &&
        typeof G.BT[G.bx[mId]] !== "undefined" &&
        (G.FT[G.fx[mId]][1] + G.BT[G.bx[mId]][1]) / dj <= ub
      ) {
        if (isSo[G.fx[mId]]) {
          cId = mId;
          var bpId = G.BT[G.bx[mId]][0][1];
          while (isSo[G.fx[bpId]]) {
            if (G.FT[G.fx[bpId]][0][1] === cId) {
              isSo[G.fx[cId]] = false;
              isSo[G.fx[bpId]] = false;
              plNodes++;
              if (plNodes === 2) {
                head = cId;
                tail = bpId;
              } else {
                tail = bpId;
              }
              cId = bpId;
              bpId = G.BT[G.bx[cId]][0][1];
            } else {
              isSo[G.fx[cId]] = false;
              break;
            }
          }

          let l = G.FT[G.fx[cId]][1] + G.BT[G.bx[cId]][1];
          if (plNodes >= 2) {
            heap.push([[head, tail], l]);
          } else {
            let m = G.FT[G.fx[cId]][0][1];
            let n = G.BT[G.bx[cId]][0][1];
            if (this.isTaut(G.verts[m].x, G.verts[m].y, G.verts[cId].x, G.verts[cId].y, G.verts[n].x, G.verts[n].y))
              heap.push([[cId, cId], l]);
          }
        }
      }
      plNodes = 1;
    }

    for (let i = 0; i < G.verts.length; ++i) isSo[i] = true;
    sp.forEach((e) => (isSo[e] = false));
    while (!heap.empty()) {
      if (tuo.length >= tk) break;
      let h = heap.peek();
      let u = h[0][0];
      heap.pop();
      if (isSo[u]) {
        let len = h[1];
        let lst = G.route(st.s, u, st.t);
        let isInsert = true;
        let curol = [];
        for (let i = 0; i < tuo.length; i++) {
          let opv = G.overlapVal(lst, len, tuo[i], costs[i]);
          curol.push(opv);
          if (opv > val) {
            isInsert = false;
            break;
          }
        }

        if (isInsert) {
          res[1].push.apply(res[1], curol);
          lst.forEach((l) => (isSo[l] = false));
          tuo.push(lst);
          res[0].push(len);
          costs.push(len);
        }
      }
    }

    for (var i = 0; i < tuo.length; ++i) {
      var ap = [];
      var p = tuo[i];
      for (var j of p) {
        ap.push(G.verts[j].x, G.verts[j].y);
      }
      out.push(ap);
    }

    // console.log("sim overlap: ", res[1]);

    G.treeClear();
    return res;
  }
};

proto.pen = function (sx, sy, tx, ty, out, val, tk, ub) {
  this.graph.treeClear();
  let res = {};
  res[0] = [];
  res[1] = [];
  if (this.grid.get(tx, ty) != 0 || this.grid.get(sx, sy) != 0) {
    return res;
  } else {
    let G = this.graph;
    G.setSourceAndTarget(sx, sy, tx, ty);
    let st = this.addST(sx, sy, tx, ty);
    let sp = [];
    let tuo = [];
    let costs = [];

    // Get optimal path from G
    let f = false;
    let dj = G.findSP(st.s, st.t, sp, f);

    if (dj === 0) return res;

    let apxy = [];
    for (let j of sp) {
      apxy.push(G.verts[j].x, G.verts[j].y);
    }
    out.push(apxy);
    res[0].push(dj);
    tuo.push(sp);
    costs.push(dj);

    // deep copy G to IG
    var Q = new Graph();
    for (let n of G.verts) {
      var nx = Q.vertex(n.x, n.y);
      Q.addVertex(nx);
    }
    for (let k of Object.keys(G.adjList)) {
      let adjs = G.adjList[k];
      for (let e of adjs) {
        let ex = ege.createEdge(e.m, e.n, e.weight);
        Q.adjList[k].push(ex);
      }
    }
    for (let k of Object.keys(G.stList)) {
      let adjs = G.stList[k];
      for (let e of adjs) {
        let ex = ege.createEdge(e.m, e.n, e.weight);
        Q.adjList[k].push(ex);
      }
    }

    // update edges weight in IG
    Q.weightUp(sp, val);

    let ot = 0;
    while (out.length < tk) {
      let p = [];
      let ap = [];

      // {
      let curDist = 0;
      let isVisited = [];
      let pc = [];
      for (let i = 0; i < Q.verts.length; ++i) {
        isVisited[i] = false;
        pc[i] = -1;
      }

      let heap = new Heap(function (a, b) {
        return a[1] - b[1];
      });

      heap.push([[st.s, st.s], 0]);
      while (!heap.empty()) {
        let mini = heap.pop();
        let uid = mini[0][0];
        let pid = mini[0][1];
        let dist = mini[1];

        if (!isVisited[uid]) {
          isVisited[uid] = true;
          pc[uid] = pid;
          if (uid === st.t) {
            curDist = dist;
            break;
          }

          let adjs = [];
          if (Q.adjList[uid]) {
            Q.adjList[uid].forEach((e) => adjs.push(e));
          }

          for (let i = 0; i < adjs.length; ++i) {
            let vid = adjs[i].other(uid);
            if (!isVisited[vid]) {
              if (uid === st.s) {
                let newDist = dist + adjs[i].weight;
                heap.push([[vid, uid], newDist]);
              } else if (
                this.isTaut(
                  G.verts[pid].x,
                  G.verts[pid].y,
                  G.verts[uid].x,
                  G.verts[uid].y,
                  G.verts[vid].x,
                  G.verts[vid].y
                )
              ) {
                let newDist = dist + adjs[i].weight;
                heap.push([[vid, uid], newDist]);
              }
            }
          }
        }
      }

      let w = st.t;
      while (w != -1) {
        if (w === st.s || w >= Q.verts.length) break;
        p.push(w);
        w = pc[w];
      }
      p.push(st.s);
      // }

      if (curDist === 0) break;

      let pLen = G.weightSum(p);
      if (pLen / dj > ub) break;

      let isInsert = true;
      let curol = [];
      for (let i = 0; i < tuo.length; i++) {
        if (tuo[i].join(",") == p.join(",")) {
          isInsert = false;
          break;
        } else {
          let opv = G.overlapVal(p, pLen, tuo[i], costs[i]);
          curol.push(opv);
        }
      }

      if (isInsert) {
        res[1].push.apply(res[1], curol);
        for (let j of p) {
          ap.push(Q.verts[j].x, Q.verts[j].y);
        }
        out.push(ap);
        tuo.push(p);
        costs.push(pLen);
        res[0].push(pLen);
      } else {
        ++ot;
      }
      Q.weightUp(p, val);

      if (ot == 3) break;
    }

    // console.log("overlap list:", res[1]);

    G.treeClear();
    return res;
  }
};

proto.topRightOfBlockedTile = function (x, y) {
  return isBlocked(x - 1, y - 1, this.grid);
};

proto.topLeftOfBlockedTile = function (x, y) {
  return isBlocked(x, y - 1, this.grid);
};

proto.bottomRightOfBlockedTile = function (x, y) {
  return isBlocked(x - 1, y, this.grid);
};

proto.bottomLeftOfBlockedTile = function (x, y) {
  return isBlocked(x, y, this.grid);
};

proto.isTautFromBottomLeft = function (x1, y1, x2, y2, x3, y3) {
  if (x3 < x2 || y3 < y2) return false;
  var compareGradients = (y2 - y1) * (x3 - x2) - (y3 - y2) * (x2 - x1); // m1 - m2
  if (compareGradients < 0) {
    // m1 < m2
    return this.bottomRightOfBlockedTile(x2, y2);
  } else if (compareGradients > 0) {
    // m1 > m2
    return this.topLeftOfBlockedTile(x2, y2);
  } else {
    // m1 == m2
    return true;
  }
};

proto.isTautFromTopLeft = function (x1, y1, x2, y2, x3, y3) {
  if (x3 < x2 || y3 > y2) return false;

  var compareGradients = (y2 - y1) * (x3 - x2) - (y3 - y2) * (x2 - x1); // m1 - m2
  if (compareGradients < 0) {
    // m1 < m2
    return this.bottomLeftOfBlockedTile(x2, y2);
  } else if (compareGradients > 0) {
    // m1 > m2
    return this.topRightOfBlockedTile(x2, y2);
  } else {
    // m1 == m2
    return true;
  }
};

proto.isTautFromLeft = function (x1, y1, x2, y2, x3, y3) {
  if (x3 < x2) return false;

  var dy = y3 - y2;
  if (dy < 0) {
    // y3 < y2
    return this.topRightOfBlockedTile(x2, y2);
  } else if (dy > 0) {
    // y3 > y2
    return this.bottomRightOfBlockedTile(x2, y2);
  } else {
    // y3 == y2
    return true;
  }
};

proto.isTautFromBottomRight = function (x1, y1, x2, y2, x3, y3) {
  if (x3 > x2 || y3 < y2) return false;
  var compareGradients = (y2 - y1) * (x3 - x2) - (y3 - y2) * (x2 - x1); // m1 - m2
  if (compareGradients < 0) {
    // m1 < m2
    return this.topRightOfBlockedTile(x2, y2);
  } else if (compareGradients > 0) {
    // m1 > m2
    return this.bottomLeftOfBlockedTile(x2, y2);
  } else {
    // m1 == m2
    return true;
  }
};

proto.isTautFromTopRight = function (x1, y1, x2, y2, x3, y3) {
  if (x3 > x2 || y3 > y2) return false;

  var compareGradients = (y2 - y1) * (x3 - x2) - (y3 - y2) * (x2 - x1); // m1 - m2
  if (compareGradients < 0) {
    // m1 < m2
    return this.topLeftOfBlockedTile(x2, y2);
  } else if (compareGradients > 0) {
    // m1 > m2
    return this.bottomRightOfBlockedTile(x2, y2);
  } else {
    // m1 == m2
    return true;
  }
};

proto.isTautFromRight = function (x1, y1, x2, y2, x3, y3) {
  if (x3 > x2) return false;

  var dy = y3 - y2;
  if (dy < 0) {
    // y3 < y2
    return this.topLeftOfBlockedTile(x2, y2);
  } else if (dy > 0) {
    // y3 > y2
    return this.bottomLeftOfBlockedTile(x2, y2);
  } else {
    // y3 == y2
    return true;
  }
};

proto.isTautFromBottom = function (x1, y1, x2, y2, x3, y3) {
  if (y3 < y2) return false;

  var dx = x3 - x2;
  if (dx < 0) {
    // x3 < x2
    return this.topRightOfBlockedTile(x2, y2);
  } else if (dx > 0) {
    // x3 > x2
    return this.topLeftOfBlockedTile(x2, y2);
  } else {
    // x3 == x2
    return true;
  }
};

proto.isTautFromTop = function (x1, y1, x2, y2, x3, y3) {
  if (y3 > y2) return false;

  var dx = x3 - x2;
  if (dx < 0) {
    // x3 < x2
    return this.bottomRightOfBlockedTile(x2, y2);
  } else if (dx > 0) {
    // x3 > x2
    return this.bottomLeftOfBlockedTile(x2, y2);
  } else {
    // x3 == x2
    return true;
  }
};

proto.isTaut = function (x1, y1, x2, y2, x3, y3) {
  if (x1 < x2) {
    if (y1 < y2) {
      return this.isTautFromBottomLeft(x1, y1, x2, y2, x3, y3);
    } else if (y2 < y1) {
      return this.isTautFromTopLeft(x1, y1, x2, y2, x3, y3);
    } else {
      // y1 == y2
      return this.isTautFromLeft(x1, y1, x2, y2, x3, y3);
    }
  } else if (x2 < x1) {
    if (y1 < y2) {
      return this.isTautFromBottomRight(x1, y1, x2, y2, x3, y3);
    } else if (y2 < y1) {
      return this.isTautFromTopRight(x1, y1, x2, y2, x3, y3);
    } else {
      // y1 == y2
      return this.isTautFromRight(x1, y1, x2, y2, x3, y3);
    }
  } else {
    // x2 == x1
    if (y1 < y2) {
      return this.isTautFromBottom(x1, y1, x2, y2, x3, y3);
    } else if (y2 < y1) {
      return this.isTautFromTop(x1, y1, x2, y2, x3, y3);
    }
  }
};

proto.isPathTaut = function (path) {
  for (var i = 0; i + 2 < path.length; i += 2) {
    var x1 = path[i];
    var y1 = path[i + 1];
    var x2 = path[i + 2];
    var y2 = path[i + 3];
    var x3 = path[i + 4];
    var y3 = path[i + 5];
    if (!this.isTaut(x1, y1, x2, y2, x3, y3)) return false;
  }
  return true;
};

function isBlocked(x, y, grid) {
  if (x >= grid.shape[0] || y >= grid.shape[1]) return true;
  if (x < 0 || y < 0) return true;
  if (grid.get(x, y) === 0) return false;
  else return true;
}

function rayTrace(x1, y1, x2, y2, grid) {
  var dy = y2 - y1;
  var dx = x2 - x1;

  var f = 0;

  var signY = 1;
  var signX = 1;
  var offsetX = 0;
  var offsetY = 0;

  if (dy < 0) {
    dy *= -1;
    signY = -1;
    offsetY = -1;
  }
  if (dx < 0) {
    dx *= -1;
    signX = -1;
    offsetX = -1;
  }

  if (dx >= dy) {
    while (x1 != x2) {
      f += dy;
      if (f >= dx) {
        if (isBlocked(x1 + offsetX, y1 + offsetY, grid)) return false;
        y1 += signY;
        f -= dx;
      }
      if (f != 0 && isBlocked(x1 + offsetX, y1 + offsetY, grid)) return false;
      if (dy == 0 && isBlocked(x1 + offsetX, y1, grid) && isBlocked(x1 + offsetX, y1 - 1, grid)) return false;

      x1 += signX;
    }
  } else {
    while (y1 != y2) {
      f += dx;
      if (f >= dy) {
        if (isBlocked(x1 + offsetX, y1 + offsetY, grid)) return false;
        x1 += signX;
        f -= dy;
      }
      if (f != 0 && isBlocked(x1 + offsetX, y1 + offsetY, grid)) return false;
      if (dx == 0 && isBlocked(x1, y1 + offsetY, grid) && isBlocked(x1 - 1, y1 + offsetY, grid)) return false;

      y1 += signY;
    }
  }
  return true;
}

function createPlanner(grid) {
  let geom = createGeometry(grid);
  let graph = new Graph();
  let E = 0;

  function createVis(corners) {
    var cl = [];
    for (var k = 0; k < corners.length; ++k) {
      var ux = graph.vertex(corners[k][0], corners[k][1]);
      graph.addVertex(ux);
      cl[grid.index(corners[k][0], corners[k][1])] = 1;
    }

    for (var i = 0; i < corners.length; ++i) {
      var u = corners[i];
      for (var j = i + 1; j < corners.length; ++j) {
        var v = corners[j];
        if (rayTrace(u[0], u[1], v[0], v[1], grid)) {
          var dist = eucliDist(u[0], u[1], v[0], v[1]);

          var x0 = u[0],
            y0 = u[1],
            isEdge = true;
          var dx = v[0] - x0,
            dy = v[1] - y0;
          var steps = Math.max(Math.abs(dx), Math.abs(dy));
          for (var z = 1; z < steps; z++) {
            if ((z * dx) % steps == 0 && (z * dy) % steps == 0) {
              var x = x0 + (z * dx) / steps,
                y = y0 + (z * dy) / steps;
              if (cl[grid.index(x, y)]) isEdge = false;
            }
          }
          if (isEdge) {
            graph.addEdge(i, j, dist);
            ++E;
          }
        }
      }
    }
  }

  createVis(geom.corners);
  graph.V = geom.corners.length;
  graph.E = E;

  // console.log("Nodes: " + graph.V + " Edges: " + E);

  return new PathPlanner(geom, graph, grid);
}
