"use strict";

module.exports = Graphx;

var ege = require("./edge.js");
var vtx = require("./vertexx.js");
var Heap = require("heap");

function Graphx() {
  this.V = 0;
  this.E = 0;
  this.verts = [];
  this.adjList = {};
  this.stList = {};

  this.BT = [];
  this.bx = [];

  this.FT = [];
  this.fx = [];
}

var proto = Graphx.prototype;

proto.vertex = function (x, y) {
  var v = vtx.create(x, y);
  return v;
};

proto.addVertex = function (u) {
  this.verts.push(u);
  var i = this.verts.length - 1;
  if (!this.adjList[i]) this.adjList[i] = [];
};

proto.addEnd = function (u) {
  this.verts.push(u);
  var i = this.verts.length - 1;
  if (!this.stList[i]) this.stList[i] = [];

  return i;
};

proto.incEdges = function (i, j, w) {
  var e = ege.createEdge(i, j, w);
  if (!this.stList[i]) this.stList[i] = [];
  if (!this.stList[j]) this.stList[j] = [];
  this.stList[i].push(e);
  this.stList[j].push(e);
};

proto.addEdge = function (i, j, w) {
  var e = ege.createEdge(i, j, w);
  // var e = [i, j, w]
  this.adjList[i].push(e);
  this.adjList[j].push(e);
};

proto.setSourceAndTarget = function (sx, sy, tx, ty) {
  this.srcX = sx | 0;
  this.srcY = sy | 0;
  this.dstX = tx | 0;
  this.dstY = ty | 0;
};

proto.eucDist = function eucliDist(x0, y0, x1, y1) {
  var dx = Math.abs(x1 - x0);
  var dy = Math.abs(y1 - y0);
  var num = Math.sqrt(dx * dx + dy * dy);
  return Number(Math.round(num + "e7") + "e-7");
};

proto.treeClear = function () {
  this.BT = [];
  this.bx = [];
  this.FT = [];
  this.fx = [];

  if (Object.keys(this.stList).length != 0) {
    for (var k of Object.keys(this.stList)) this.stList[k] = [];
  }

  var n = this.verts.length - this.V;
  for (var i = 0; i < n; ++i) {
    this.verts.pop();
    if (this.verts.length === this.V) break;
  }
};

proto.findSP = function (s_i, t_i, out, f) {
  var dt = 0;
  var isVisited = [];
  var pc = [];
  for (var i = 0; i < this.verts.length; ++i) {
    isVisited[i] = false;
    pc[i] = -1;
  }

  var heap = new Heap(function (a, b) {
    return a[1] - b[1];
  });

  heap.push([[s_i, s_i], 0]);
  while (!heap.empty()) {
    var mini = heap.pop();
    var uid = mini[0][0];
    var pid = mini[0][1];
    var dist = mini[1];

    if (!isVisited[uid]) {
      isVisited[uid] = true;
      pc[uid] = pid;
      if (uid === t_i) {
        dt = dist;
        break;
      }

      var adjs = [];
      if (this.adjList[uid]) {
        this.adjList[uid].forEach((e) => adjs.push(e));
      }
      if (this.stList[uid]) {
        this.stList[uid].forEach((e) => adjs.push(e));
      }

      // var adjs = this.adjList[uid]
      for (var i = 0; i < adjs.length; ++i) {
        var vid = adjs[i].other(uid);
        if (!isVisited[vid]) {
          var newDist = dist + adjs[i].weight;
          heap.push([[vid, uid], newDist]);
        }
      }
    }
  }

  var w = t_i;
  while (w != -1) {
    if (w === s_i || w >= this.verts.length) break;
    if (f) {
      out.push(this.verts[w].x);
      out.push(this.verts[w].y);
    } else out.push(w);
    w = pc[w];
  }
  if (f) {
    out.push(this.verts[s_i].x);
    out.push(this.verts[s_i].y);
  } else out.push(s_i);

  return dt;
};

proto.backTree = function (s_i, t_i, ub) {
  for (var i = 0; i < this.verts.length; ++i) this.bx[i] = -1;

  var heap = new Heap(function (a, b) {
    return a[1] - b[1];
  });

  var opCost = Number.MAX_VALUE;

  heap.push([[t_i, t_i], 0]);
  while (!heap.empty()) {
    var m = heap.peek();
    var uid = m[0][0];
    var pid = m[0][1];
    var dist = m[1];
    heap.pop();

    if (this.bx[uid] === -1) {
      this.bx[uid] = this.BT.length;
      this.BT.push([[uid, pid], dist]);

      if (uid === s_i) opCost = dist;
      if (dist / ub > opCost) break;

      var adjs = [];
      if (this.adjList[uid]) this.adjList[uid].forEach((e) => adjs.push(e));
      if (this.stList[uid]) this.stList[uid].forEach((e) => adjs.push(e));

      for (var i = 0; i < adjs.length; ++i) {
        var vid = adjs[i].other(uid);
        if (this.bx[vid] === -1) {
          var newDist = dist + adjs[i].weight;
          heap.push([[vid, uid], newDist]);
        }
      }
    }
  }
};

proto.forwardTree = function (s_i, t_i, ub) {
  for (var i = 0; i < this.verts.length; ++i) this.fx[i] = -1;

  var heap = new Heap(function (a, b) {
    return a[1] - b[1];
  });

  var opCost = Number.MAX_VALUE;

  heap.push([[s_i, s_i], 0]);
  while (!heap.empty()) {
    var m = heap.peek();
    var uid = m[0][0];
    var pid = m[0][1];
    var dist = m[1];
    heap.pop();

    if (this.fx[uid] === -1) {
      this.fx[uid] = this.FT.length;
      this.FT.push([[uid, pid], dist]);

      if (uid === t_i) opCost = dist;
      if (dist / ub > opCost) break;

      var adjs = [];
      if (this.adjList[uid]) this.adjList[uid].forEach((e) => adjs.push(e));
      if (this.stList[uid]) this.stList[uid].forEach((e) => adjs.push(e));

      for (var i = 0; i < adjs.length; ++i) {
        var vid = adjs[i].other(uid);
        if (this.fx[vid] === -1) {
          var newDist = dist + adjs[i].weight;
          heap.push([[vid, uid], newDist]);
        }
      }
    }
  }
};

proto.path = function (sId, uId, tId) {
  var arr = [];
  var u = uId;
  while (u != sId) {
    u = this.FT[this.fx[u]][0][1];
    arr.push(this.verts[u].x, this.verts[u].y);
  }

  u = uId;
  arr.unshift(this.verts[u].x, this.verts[u].y);
  while (u != tId) {
    u = this.BT[this.bx[u]][0][1];
    arr.unshift(this.verts[u].x, this.verts[u].y);
  }

  return arr;
};

proto.route = function (sId, uId, tId) {
  var arr = [];
  var u = uId;
  while (u != sId) {
    u = this.FT[this.fx[u]][0][1];
    arr.push(u);
  }

  u = uId;
  arr.unshift(u);
  while (u != tId) {
    u = this.BT[this.bx[u]][0][1];
    arr.unshift(u);
  }

  return arr;
};

// proto.isOverlap = function (p1, c1, p2, c2, val) {
// let op = 0;
// for (let j = 1; j < p2.length; ++j) {
// let cn = p2[j];
// let pn = p2[j - 1];

// if (p1.indexOf(pn) + 1 === p1.indexOf(cn)) {
// var adjs = [];
// if (this.adjList[cn]) this.adjList[cn].forEach((e) => adjs.push(e));
// if (this.stList[cn]) this.stList[cn].forEach((e) => adjs.push(e));
// for (var e of adjs) {
// var vid = e.other(cn);
// if (vid === pn) {
// op += e.weight;
// break;
// }
// }
// }
// }
// // var c3 = Math.min(c1, c2);
// var c3 = c1 + c2 - op;
// // console.log("simi: ", c1, c2, op, c3, op / c3);
// if (op / c3 > val) return true;
// else return false;
// };

proto.overlapVal = function (p1, c1, p2, c2) {
  let op = 0;
  for (let j = 1; j < p2.length; ++j) {
    let cn = p2[j];
    let pn = p2[j - 1];

    if (p1.indexOf(pn) + 1 === p1.indexOf(cn)) {
      // console.log(" c and p: ", pn, cn);
      let adjs = [];
      if (this.adjList[cn]) this.adjList[cn].forEach((e) => adjs.push(e));
      if (this.stList[cn]) this.stList[cn].forEach((e) => adjs.push(e));
      for (var e of adjs) {
        var vid = e.other(cn);
        if (vid === pn) {
          op += e.weight;
          break;
        }
      }
    }
  }

  let x = op / (c1 + c2 - op);
  // // let x = op / Math.min(c1, c2);
  // console.log(c1, p1);
  // console.log(c2, p2);
  // console.log(op, c1 + c2 - op);
  return Number.parseFloat(x).toFixed(4);
  // return op / (c1 + c2 - op);
};

proto.weightUp = function (sp, val) {
  for (let i = 1; i < sp.length; ++i) {
    let c = sp[i];
    let p = sp[i - 1];

    let adjs = [];
    if (this.adjList[c]) this.adjList[c].forEach((e) => adjs.push(e));
    for (let e of adjs) {
      if (e.other(c) == p) {
        let z = e.weight * val;
        e.setWeight(z);
        break;
      }
    }

    let adjt = [];
    if (this.adjList[p]) this.adjList[p].forEach((e) => adjt.push(e));
    for (let e of adjt) {
      if (e.other(p) == c) {
        let z = e.weight * val;
        e.setWeight(z);
        break;
      }
    }
  }
};

proto.weightSum = function (sp) {
  let dist = 0;
  for (let i = 1; i < sp.length; ++i) {
    let c = sp[i];
    let adjs = [];
    if (this.adjList[c]) this.adjList[c].forEach((e) => adjs.push(e));
    if (this.stList[c]) this.stList[c].forEach((e) => adjs.push(e));

    let p = sp[i - 1];
    for (let e of adjs) {
      if (e.other(c) == p) {
        dist += e.weight;
        break;
      }
    }
  }
  return dist;
};
